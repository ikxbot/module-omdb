<?php
namespace Ikx\OMDB\Command;

use Ikx\Core\Application;
use Ikx\Core\Command\AbstractCommand;
use Ikx\Core\Command\CommandInterface;
use Ikx\Core\Utils\MessagingTrait;

class MovieCommand extends AbstractCommand implements CommandInterface {
    use MessagingTrait;

    public function describe()
    {
        return "Fetch information about a movie or tv show";
    }

    public function run() {
        if (isset($this->params[0])) {
            $apiKey = Application::config()->get('options.omdb.key');

            $title = join(' ', $this->params);
            $params = [
                'apikey'        => $apiKey,
                't'             => $title,
                'plot'          => 'short',
                'r'             => 'json'
            ];

            $url = 'http://www.omdbapi.com/?' . http_build_query($params);
            $contents = file_get_contents($url);

            if (!$contents) {
                $this->msg($this->target, 'Something went wrong while fetching movie info.');
            } else {
                $json = json_decode($contents);

                if ($json->Error) {
                    $this->msg($this->channel, sprintf('Error: %s', $json->Error));
                } else {
                    $this->msg($this->channel, sprintf("\x02%s (%s)\x02 %s %s", $json->Title, $json->Year, $json->Genre, $json->Rated));
                    $this->msg($this->channel, sprintf('%s', $json->Plot));
                    $this->msg($this->channel, sprintf('Cast: %s', $json->Actors));
                    $this->msg($this->channel, sprintf('Awards: %s', $json->Awards));
                    $this->msg($this->channel, sprintf('Language: %s / Country: %s / Runtime: %s', $json->Language, $json->Country, $json->Runtime));
                    $this->msg($this->channel, sprintf('IMDb rating: %s / Metascore: %s', $json->imdbRating, $json->Metascore));
                    $this->msg($this->channel, sprintf('More info: https://www.imdb.com/title/%s/', $json->imdbID));
                }
            }

        }
    }
}